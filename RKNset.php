#!/usr/bin/env php
<?php
// Copyright 2018 Alex D (https://gitlab.com/Nooblord/)
// This is free software, licensed under the GNU General Public License v3.

// check if run from cli
if (PHP_SAPI != "cli") exit;

// Set 2G memory limit
ini_set('memory_limit','2G');

// helper functions
function list_header($name,$type='single')
{
    if ($type=='single')
        return "create $name hash:ip family inet maxelem 65536\n";
    else
        return "create $name hash:net family inet maxelem 65536\n";
}

function list_add($name,$value)
{
    return "add $name $value\n";
}

function build_ipset($type, $name_prefix, array &$address_list, array &$sets)
{
    $i=1;
    $list_num=1;
    $header_in=false;
    while (count($address_list)>0) {
        $listname=($list_num==1)?$name_prefix:($name_prefix.$list_num);
        if (!$header_in) {
            $sets[$listname]=list_header($listname, $type);
            $header_in=true;
        }
        $ipadd=list_add($listname,array_pop($address_list));
        $sets[$listname].=$ipadd;
        if ($i==65536 && count($address_list)>0) {
            $i=0;
            $header_in=false;
            $list_num++;
        }
        $i++;
}
}

// Create lists
$iplist_single=[];
$iplist_subnet=[];
$iplist_lite_single=[];
$iplist_lite_subnet=[];

// Get list from zapret-info github
$blacklist=file_get_contents('https://raw.githubusercontent.com/zapret-info/z-i/master/dump.csv');

// Convert somewhat to just csv
//$blacklist = mb_convert_encoding($blacklist,'UTF-8','CP1251');
$blacklist = preg_replace("/^(.*\n){1}/", "",$blacklist);

// Extract single ip and subnets
foreach (explode("\n",$blacklist) as $row) {
    $ips = strtok($row, ";");
    foreach (explode(" | ", $ips) as $ip) {
        if (strpos($ip, "/")) {
            if(filter_var(explode("/",$ip)[0], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4))
                $iplist_subnet[]=$ip;
        }
        else {
            if(filter_var($ip,FILTER_VALIDATE_IP, FILTER_FLAG_IPV4))
                $iplist_single[]=$ip;
        }
    }
}

// Get list from antifilter.download
$blacklist=file_get_contents('https://antifilter.download/list/ip.lst');
$blacklist.=file_get_contents('https://antifilter.download/list/subnet.lst');
foreach (explode("\n",$blacklist) as $ip) {
        if (strpos($ip, "/")) {
            if(filter_var(explode("/",$ip)[0], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4))
                $iplist_subnet[]=$ip;
        }
        else {
            if(filter_var($ip,FILTER_VALIDATE_IP, FILTER_FLAG_IPV4))
                $iplist_single[]=$ip;
        }
}

$iplist_single=array_filter(array_unique($iplist_single));
$iplist_subnet=array_filter(array_unique($iplist_subnet));

// Get light version of list from antifilter.download
$blacklist=file_get_contents('https://antifilter.download/list/ipsum.lst');
foreach (explode("\n",$blacklist) as $ip) {
        if (strpos($ip, "/")) {
            if(filter_var(explode("/",$ip)[0], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4))
                $iplist_lite_subnet[]=$ip;
        }
        else {
            if(filter_var($ip,FILTER_VALIDATE_IP, FILTER_FLAG_IPV4))
                $iplist_lite_single[]=$ip;
        }
}

$iplist_lite_single=array_filter(array_unique($iplist_lite_single));
$iplist_lite_subnet=array_filter(array_unique($iplist_lite_subnet));

$ipsets=[];

//  Build sets for full version of list
build_ipset('single', 'rkn', $iplist_single, $ipsets);
build_ipset('subnet', 'rkn_net', $iplist_subnet, $ipsets);

// Build sets for light version of list
build_ipset('single', 'rkn_lite', $iplist_lite_single, $ipsets);
build_ipset('subnet', 'rkn_lite_net', $iplist_lite_subnet, $ipsets);


foreach ($ipsets as $setname => $value) {
    file_put_contents($setname.".ipset", $value);
    if (strpos($setname, 'rkn_lite') === false) file_put_contents('rkn.sets',$setname.".ipset\n", FILE_APPEND);
    if (strpos($setname, 'rkn_') !== false) file_put_contents('rkn_lite.sets',$setname.".ipset\n", FILE_APPEND);
}

echo "Memory used: ".(memory_get_peak_usage(true)/1024/1024)." MiB"

?>
